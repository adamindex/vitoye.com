---
title: React dan Nodejs
description: Di tutorial ini kita akan belajar cara serve aplikasi React bersama Node.js dan Express.js.
img: "https://images.unsplash.com/photo-1607799279861-4dd421887fb3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2158&q=80"
alt: Full-Stack Development
author:
  name: Adam Maulana
  bio: Pro Stack Overflow copy-paster
  img: static/images/avatar.png
---

## Full Stack Development Starter 🏫

Ketika mulai beralih dari desainer grafis menjadi programmer, gue sendiri sama sekali tak tahu apa yang harus di lakuin. Yang lebih parahnya lagi gue tidak punya mentor atau panduan apa pun tentang cara-cara coding. Semua kolega kebanyakan orang marketer dan belum pernah sama sekali buat aplikasi web dengan standar industri yang baik. Lalu pada akhirnya kemauan gue sendirilah yang membuka pintu dengan memperhatikan trend teknologi terbaru dan juga belajar cara implementasinya.

Tanpa masih gak tahu apa-apa.. bahkan gue terus Googling dan hampir gak menemukan gambaran tentang hal apa yang dibutuhkan untuk men-develop aplikasi full stack, gue terus cari info teknologi lebih dalam lagi untuk menemukan path gue sendiri. Setelah berminggu-minggu meneliti tentang bahasa pemograman dan berbagai macam platform hosting, pada akhirnya gue bisa membuat aplikasi fullstack yang sudah finish dan bisa running di server Node.js dan Angular 2+.

Gue sendiri akhirnya juga sering bertanya-tanya seperti apa bentuk project, atau perkembangan teknologi di masa mendatang, untuk sekalian gue mencerna semua informasi dan sambil mempelajari semua trend. Itulah mengapa tujuan gue membuat artikel ini - ialah untuk memulai sebagai mentor kecil yang membantu kalian untuk memulai belajar.

### React dan Node.js

Kita awali dengan yang namanya fullstack javascript. Teknologi ini dapat membuat development dan implementasi aplikasi full-stack jadi mudah karena kalian hanya perlu mengetahui satu bahasa pemograman yaitu javascript.

Note: Gue pada dasarnya adalah javascript developer, tetapi mempelajari bahasa scripting seperti Python atau Bash sangatlah bermanfaat. Dan jika kalian menerapkannya, Javascript tetap akan berfungsi.

Ada beberapa cara untuk menyusun folder client dan server, tetapi untuk contoh kali ini, kita akan buat sesederhana mungkin! Dengan begitu, kalian dapat mengembangkannya lagi sendiri.

** requirements **
Node.js sudah terinstall - [Kalian dapat mengunduhnya di sini.](https://nodejs.org/en/download/)

### Siap mulai!

Hal yang pertama - mari buat aplikasi React:

```bash
npx create-react-app react-node
cd react-node
yarn build
```

Jika kalian belum pernah menggunakan npx sebelumnya - ini pada dasarnya adalah library eksekusi. Jadi tanpa harus menginstal create-react-app secara global dan kemudian menggunakan perintah global untuk membuat aplikasi, disini kalian cukup menggunakan npx saja.

Pada tahap ini, aplikasi React siap digunakan! Untuk sekarang belum bisa melakukan apa-apa karena yang perlu kita lakukan adalah memulai serve dari server.

Kita perlu menambahkan Express ke project dan kemudian membuat file. Gue akan menamai file ini server.js.

```bash
yarn add express
touch server.js
```

Sekarang, buka file dan paste baris code ini:

```js
const express = require("express");
// instantiates express so we can use the middleware functions
const app = express();

// Node’s native tool for working with files.
const path = require("path");

// set a default port in case the host isn’t configured with one
const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, "build")));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "build/index.html"));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
```

Karena file tersebut adalah file javascript dan kita akan menggunakan perintah <code>node</code> untuk menjalankannya, runtime pun di jalankan oleh Node.js.

Pada baris 2 itu sudah benar kita siapkan "app" sebagai aplikasi Ekspres kita. Sama seperti REST requests, Express memiliki fungsi <code>get</code>, <code>post</code>, <code>put</code>, dan <code>delete</code>. Tetapi jika kalian ingin menggunakan satu fungsi middleware untuk semua HTTP verbs, fungsi <code>use</code> adalah pilihan.

Pada baris 6, aplikasi Express meload yang ada di folder build. Tanpa baris ini, code akan gagal karena Express tidak akan mengirimkan file index.html ke browser.

Fungsi <code>app.use (...)</code> inilah yang sebenarnya yang serve aplikasi dari root React kita ke browser. Perhatikan bahwa ini hanya di serve pada saat ada GET request, lalu serving filenya pun di semua rute. Dengan cara ini aplikasi React kita mulai di routing, server mengembalikan file index.html dan lalu memastikan aplikasi React berjalan.

Untuk serving aplikasi, pastikan sudah berada di root project dan ketik di terminal:

```bash
node server.js
```

Whala! Aplikasi full stack - selesai. sudah bisa mulai serve aplikasi React yang sudah dibuat dengan Express.
Tapi bisa kita tambahkan route lain lagi agar menjadi lebih pasti dalam proses pembelajaran Full-stack development.

Pertama, tambahkan library body-parser.

```bash
yarn add body-parser
```

Sekarang mari kita impor dan atur route baru:

```js
const express = require('express');
const app = express();
+ const bodyParser = require('body-parser')
const path = require('path');
const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'build')));
+ app.use(bodyParser.json());

+ app.post('/new-route', (req, res) => {
+ let name = req.body.name;
+ res.send(
+ {greeting: `Hello ${name}`}
+ );
+ })

app.get('*', (req,res) => {
res.sendFile(path.join(__dirname+'build/index.html'));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
```

Rute baru ini akan mengambil POST request pada route yang matching dengan "/new-route" lalu mengembalikan object greeting. Perhatikan bahwa kita juga telah menambahkan fungsi <code>app.use (...)</code> ini fungsi lain yang akan memparse value body dalam req object.

Sekarang ke React!

```js
import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
const [greeting, setGreeting] = useState('');

+ fetch("/new-route", {
+ method: 'POST',
+ headers: {
+ 'Content-Type': 'application/json',
+ },
+ body: JSON.stringify({name: 'Luke Duke'})
+ })
+ .then(res => res.json())
+ .then((result) => {
+ setGreeting(result.greeting);
+ })
+ .catch(error => console.log(error))
return (
<div className="App">
<header className="App-header">
<img src={logo} className="App-logo" alt="logo" />
<p>
Edit <code>src/App.js</code> and save to reload.
</p>
+ {greeting &&
+ <h2>{greeting}</h2>
+ }
<a
className="App-link"
href="https://reactjs.org"
target="_blank"
rel="noopener noreferrer"
>
Learn React
</a>
</header>
</div>
);
}

export default App;
```

Kita tambahkan impor "useState" dan hook untuk respon dari server. Kita juga menambahkan fungsi <code>fetch ()</code> untuk POST ke server dengan sebuah nama. JSX untuk merender greeting ketika sudah set.

Note: Klian tidak perlu mengembalikan object dari Express app. Biasanya aplikasi real world itu mengembalikan variabel non-string, tetapi kalian juga bisa return type apa pun dari Express.

Finally, rebuild React dan start up servernya untuk lihat aplikasinya bisa berfungsi!

```bash
yarn build
node server.js
```

Dan begitulah kurag lebih contoh full stack Javascript! Ada banyak sekali dokumentasi yang bagus di situs-situs resmi seperti Framework Express.js dan ini bagus untuk membantu kalian dalam proses belajar.

Header by [unsplash.com](https://unsplash.com)
