---
title: Resource untuk pengembangan web
description: Platform pembelajaran gratis, situs web, channel youtube, podcast, dan resources untuk pengembang.
img: "https://images.unsplash.com/photo-1488590528505-98d2b5aba04b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2158&q=80"
alt: Resource untuk pengembangan web
author:
  name: Adam Maulana
  bio: Pro Stack Overflow copy-paster
  img: static/images/avatar.png
---

## Platform pembelajaran gratis 🏫

- [FreeCodeCamp](http://freecodecamp.org)
- [The Odin Project](http://sololearn.com)
- [CodeAcademy](http://codecademy.com)
- [Javascript30](http://javascript30.com)
- [Font End Mentor](http://frontendmentor.io)
- [Test Automation University](http://testautomationu.applitools.com)
- [Coursera](http://coursera.org)
- [Solo Learn](http://sololearn.com)

## Channel Youtube 🎥

- [Traversy Media](https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA)
- [FreeCodeCamp](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ)
- [The Net Ninja](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg)
- [Google Chrome Developers](https://www.youtube.com/user/ChromeDevelopers)
- [The New Boston](https://www.youtube.com/user/thenewboston)
- [Derek Banas](https://www.youtube.com/user/derekbanas)
- [Academind](https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w)

## Code Editor ✏️

- [Visual Studio Code [VS Code]](https://code.visualstudio.com/)
- [Atom](https://atom.io/)
- [Sublime Text](https://www.sublimetext.com/)

## Icon 📁

- [Material Icons](http://material.io/resources/icons)
- [Flaticon](http://flaticon.com)
- [Icons8](http://icons8.com)
- [IconMonstr](http://iconmonstr.com)
- [Font Awesome](http://fontawesome.com)

## Foto 📷

- [Unsplash](http://unsplash.com)
- [Pixabay](http://pixabay.com)
- [Pezels](http://pexels.com)

## Ilustrasi 🖼️

- [Undraw](http://undraw.co/illustrations)
- [DrawKit](http://drawkit.io)
- [Icons 8](http://icons8.com/ouch)
- [IRA Desing](http://iradesign.io)
- [Interfacer](http://interfacer.xyz)
- [Blush](http://blush.design)

## Font 📰

- [Google Fonts](http://fonts.google.com)
- [Fontspace](http://fontspace.com)
- [1001 Fonts](http://1001fonts.com)
- [Font Squirrel](http://fontsquirrel.com)

## Animasi 🏃

- [CSShake](https://elrumordelaluz.github.io/csshake/)
- [Animate.css](https://animate.style/)
- [AnimeJS](https://animejs.com/)
- [GreenSock (GSAP)](https://greensock.com/)
- [Magic Animations](https://www.minimamente.com/project/magic/)
- [Hover css](https://ianlunn.github.io/Hover/)
- [AniJS](https://anijs.github.io/)
- [Wicked CSS](https://kristofferandreasen.github.io/wickedCSS/#)
- [Tuesday](https://shakrmedia.github.io/tuesday/)
- [Mo.js](https://mojs.github.io/)
- [Bounce.js](http://bouncejs.com/)

## Warna Palet 👾

- [Coolors](http://coolors.co)
- [Color Hunt](http://colorhunt.co)
- [Paletton](http://paletton.com)
- [Color Hex](http://color-hex.com)
- [MyColor](http://mycolor.space)

## Inspirasi UI 🚵

- [UI Movement](http://uimovement.com)
- [UI Garage](http://uigarage.net)
- [Collect UI](http://collectui.com)

## Podcasts 📻

- [Syntax](https://syntax.fm/)
- [Fullstack radio](https://www.fullstackradio.com/)
- [The Changelog](https://changelog.com/)
- [The Laracasts Snippet](https://laracasts.com/podcast)
- [Front End Happy Hour](https://frontendhappyhour.com/)
- [JavaScript Jabber Archives](https://devchat.tv/podcasts/js-jabber/)
- [Commit Your Code!](https://anchor.fm/commityourcode)
- [Shop Talk](https://shoptalkshow.com/)
- [Ladybug Podcast](https://www.ladybug.dev/)
- [CodePen Radio](https://blog.codepen.io/radio/)
- [JAMStack Radio](https://www.heavybit.com/library/podcasts/jamstack-radio/)
- [Modern web](https://www.thisdot.co/modern-web)

## Mengasah skill 🏆

- [Codewars](http://codewars.com)
- [Top Coder](http://topcoder.com)
- [Codingame](http://codingame.com)
- [Hacker Rank](http://hackerrank.com)
- [Project Euler](http://projecteuler.net)
- [Coder Byte](http://coderbyte.com)
- [Code Chef](http://codechef.com)
- [Exercism](http://exercism.io)
- [Leet Code](http://leetcode.com)
- [Spoj](http://spoj.com)

## Platform untuk freelancer 🔧

- [TopTotal](http://toptal.com)
- [UpWork](http://upwork.com)
- [Freelancer](http://freelancer.com)
- [People Per Hour](http://peopleperhour.com)
- [Simply Hired](http://simplyhired.com)
- [Envato](http://envato.com)
- [Guru](http://guru.com)
- [Fiverr](http://fiverr.com)
- [Hireable](http://hireable.com)
- [6 nomads](http://6nomads.com)

## Remote 📌

- [FlexJobs](http://flexjobs.com)
- [Remote](http://remote.co/remote-jobs)
- [Just Remote](http://justremote.co)
- [We Work Remotely](http://weworkremotely.com)
- [Remote OK](http://remoteok.io)
- [Jobspresso](http://jobspresso.co)
- [Europe Remotely](http://europeremotely.com)
- [WFH](http://wfh.io)

Header by [unsplash.com](https://unsplash.com)
