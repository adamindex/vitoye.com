<h1 align="center">
Personal Portfiolio in Nuxt
</h1>

<div align="center">
<img width="150" src="https://vitoye.com/pp.png" alt="Personal portfolio" />
</div>

Personal portfolio using Vue, Nuxt and Nuxt Content as CMS. Injects .md files as HTML in a vue components. Includes "blog" and "projects" sections. Light/Dark mode theming included.

[See it live here](https://vitoye.com)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production
$ npm run build

# generate static project
$ npm run export

# Run local server to test built project
$ npm run serve
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
